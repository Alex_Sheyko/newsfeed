��    #      4  /   L        �   	  	   �     �     �     �     �     �  	   �     �     �     �               1     7     <  
   D     O     V     i     y     �  <   �     �     �  	   �     �     �     �     �  *        A     F     K  �  Q  �   �  '   �     �  '   �     $  '   3     [     t     �     �     �     �      �     �     	     	     	     >	     M	  %   `	  
   �	  '   �	  v   �	     0
  )   =
     g
     z
     �
     �
     �
  1   �
  
   �
     �
                                               !         	             
                         "                      #                                              A recommended HTML5 related terms list is available <a href="http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions" target="_blank">here</a>. Add Image Align Alternate Text Caption Change Image Custom Full Size Height Image Image Widget Image Widget Plus Insert Into Widget Large Link Link ID Link Title Medium Modern Tribe, Inc. Open New Window Related Select an Image Showcase a single image with a Title, URL, and a Description Size Stay in Window Thumbnail Title Width center http://m.tri.be/iwpdoc http://wordpress.org/plugins/image-widget/ left none right PO-Revision-Date: 2020-07-08 22:29:30+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: GlotPress/3.0.0-alpha.2
Language: ru
Project-Id-Version: Plugins - Image Widget - Stable (latest release)
 Рекомендованный список связанных HTML5 терминов доступен <a href="http://microformats.org/wiki/existing-rel-values#HTML5_link_type_extensions" target="_blank">здесь</a>. Добавить изображение Выравнивание Альтернативный текст Подпись Изменить изображение Произвольный Полный Размер Высота Изображение Image Widget Image Widget Plus Вставить в виджет Большой Ссылка Id ссылки Заголовок ссылки Средний Modern Tribe, Inc. Открыть в новом окне Связь Выберите изображение Показывает одно изображение с заголовком, URL-адресом и описанием Размер Открыть в текущем окне Миниатюра Название Ширина по центру http://m.tri.be/iwpdoc http://wordpress.org/extend/plugins/image-widget/ слева нет справа 