<?php 

add_action( 'wp_enqueue_scripts' , 'bootstrap');
add_action( 'wp_enqueue_scripts' , 'script_style');
add_action('after_setup_theme','theme_register_nav_menu');
add_action('after_setup_theme','page_register_nav_menu');
add_action('widgets_init','register_my_widgets');
add_theme_support( 'post-thumbnails' );


  /*new action*/
/*functions*/

function bootstrap()
{
    wp_enqueue_style('style', get_template_directory_uri().'/assets/css/style.css', '5.7.9',true);
    wp_enqueue_style('animate', get_template_directory_uri() .'/assets/css/animate.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() .'/assets/css/bootstrap.min.css');
    wp_enqueue_style('awesome', get_template_directory_uri() .'/assets/css/font-awesome.min.css');
    wp_enqueue_style('font', get_template_directory_uri() .'/assets/css/font.css');
    wp_enqueue_style('fancybox', get_template_directory_uri() .'/assets/css/jquery.fancybox.css');
    wp_enqueue_style('scroller', get_template_directory_uri() .'/assets/css/li-scroller.css');
    wp_enqueue_style('slick', get_template_directory_uri() .'/assets/css/slick.css');
    wp_enqueue_style('theme', get_template_directory_uri() .'/assets/css/theme.css');
}
function script_style()
{

    wp_enqueue_script('bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js');
    wp_enqueue_script('custom', get_template_directory_uri().'/assets/js/custom.js');
    wp_enqueue_script('html5shiv', get_template_directory_uri().'/assets/js/html5shiv.min.js');
    wp_enqueue_script('fancybox', get_template_directory_uri().'/assets/js/jquery.fancybox.pack.js');
    wp_enqueue_script('scroller', get_template_directory_uri().'/assets/js/jquery.li-scroller.1.0.js');
    wp_enqueue_script('jquery', get_template_directory_uri().'/assets/js/jquery.min.js');
    wp_enqueue_script('newsTicker', get_template_directory_uri().'/assets/js/jquery.newsTicker.min.js');
    wp_enqueue_script('respond', get_template_directory_uri().'/assets/js/respond.min.js');
    wp_enqueue_script('slick', get_template_directory_uri().'/assets/js/slick.min.js');
    wp_enqueue_script('wow', get_template_directory_uri().'/assets/js/wow.min.js');

}
function wpb_hook_javascript() {
    ?>
        <script type="text/javascript">
        </script>
    <?php
}
function theme_register_nav_menu()
{
    register_nav_menu('top','меню в шапке');
}
function page_register_nav_menu()
{
    register_nav_menu('mid','меню в странице');
}
function register_my_widgets()
{

    register_sidebar(array(
        'name' => 'sidebar-right',
        'id' => "prev-button",
        'class' => '<div class="latest_postnav">'
    ));
    register_sidebar(array(
        'name' => 'sidebar-category',
        'id' => "prev-button",
        'class' => '<div class="right_content">'
    ));
}
function autoset_featured() {
    global $post;

    // проверка на наличие миниатюры посте
    if( has_post_thumbnail($post->ID) )
        return;

    $attached_image = get_children( array(
        'post_parent'=>$post->ID, 'post_type'=>'attachment', 'post_mime_type'=>'image', 'numberposts'=>1, 'order'=>'ASC'
    ) );

    // делаем условие проверку на наличие картинки
    if( $attached_image ){
        foreach ($attached_image as $attachment_id => $attachment)
            set_post_thumbnail($post->ID, $attachment_id);
    }
}
add_filter( 'excerpt_length', function(){
	return 0;
} );

?>