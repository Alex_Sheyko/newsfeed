<?php get_header();
wp_enqueue_scripts();?>
<body>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
<div class="container">
  <header id="header">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="header_top">
          <div class="header_top_left">
               <?php wp_nav_menu(array
                ('theme_location'=>'top',
                    'container'=>'head-bottom',
                    'menu_class'=>'navbar-collapse collapse',
                    'menu_id'=>'header',   
                    'items_wrap' => '<ul id="top_nav" class="top_nav">%3$s</ul>',       
                ));
                ?>
          </div>
          <div class="header_top_right">
            <p>Friday, December 05, 2045</p>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="header_bottom">
          <div class="logo_area"><a href="index.html" class="logo"><img src="/images/logo.jpg" alt=""></a></div>
          <div class="add_banner"><a href="#"><img src="/images/addbanner_728x90_V1.jpg" alt=""></a></div>
        </div>
      </div>
    </div>
  </header>
  <section id="navArea">
    <nav class="navbar navbar-inverse" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
         <?php wp_nav_menu(array
                ('theme_location'=>'mid',
                    'container'=>'navbar-collapse collapse',
                    'menu_class'=>'navbar-collapse collapse',
                    'menu_id'=>'navbar',   
                    'items_wrap' => '<ul id="top_nav" class="nav navbar-nav main_nav">%3$s</ul>',    
                ));
                ?>
      </div>
    </nav>
  </section>
  <section id="newsSection">
                          <!-- доработать слайдер(редактирование группы полей) и возможность добавления записей в поле) -->
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div class="latest_newsarea"> <span>Latest News</span>
<ul>
     <?php echo do_shortcode('[metaslider id="75"]'); ?>
</ul>
                         <!-- поставить сюда список соц сетей и подключить классы -->
          <div class="social_area">
            <ul class="social_nav">
              <li class="facebook"><a href="#"></a></li>
              <li class="twitter"><a href="#"></a></li>
              <li class="flickr"><a href="#"></a></li>
              <li class="pinterest"><a href="#"></a></li>
              <li class="googleplus"><a href="#"></a></li>
              <li class="vimeo"><a href="#"></a></li>
              <li class="youtube"><a href="#"></a></li>
              <li class="mail"><a href="#"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="sliderSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="slick_slider">
<?php echo do_shortcode('[metaslider id="98"]'); ?>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="latest_post">
          <h2><span>Latest post</span></h2>
          <div class="latest_post_container">
            <?php echo do_shortcode('[metaslider id="206"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="contentSection">
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-8">
        <div class="left_content"> 
          <div class="single_post_content">
            <h2><span>Business</span></h2>
            <div class="single_post_content_left">
              <ul class="business_catgnav  wow fadeInDown">
      
        <?php global $post;             
        $postslist = get_posts( array( 'category_name'=>'business' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                      <a class="featured_img"><?php the_post_thumbnail(); ?></a>
                      <h2><?php the_title(); ?></h2>
                      <p><?php the_excerpt(); ?></p>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul>
            </div>
            <div class="single_post_content_right">
              <ul class="spost_nav">
        <?php global $post;    
        $postslist = get_posts( array( 'category_name'=>'widget' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                     <div class="media wow fadeInDown"> <a class="media-left"><?php the_post_thumbnail(); ?></a> 
                     <div class="media-body"> <a class="catg_title"><?php the_title(); ?></a></div>
                    </div>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul>
            </div>
          </div>
          <div class="fashion_technology_area">
            <div class="fashion">
              <div class="single_post_content">
                <h2><span>Fashion</span></h2>
                <ul class="business_catgnav wow fadeInDown">
 <?php global $post;             
        $postslist = get_posts( array( 'category_name'=>'fashion' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                           <figure class="bsbig_fig"><a class="featured_img"> <?php the_post_thumbnail(); ?> <span class="overlay"></span> </a>
                            <figcaption> <a> <?php the_title(); ?></a> </figcaption>
                            <p><?php the_excerpt(); ?></p>
                            </figure>
                      </li>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
                </ul>
                <ul class="spost_nav">
                  <?php global $post;    
        $postslist = get_posts( array( 'category_name'=>'widget' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                     <div class="media wow fadeInDown"> <a class="media-left"><?php the_post_thumbnail(); ?></a> 
                     <div class="media-body"> <a class="catg_title"><?php the_title(); ?></a></div>
                    </div>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul>
              </div>
            </div>

            <div class="technology">
              <div class="single_post_content">
                <h2><span>Technology</span></h2>
                <ul class="business_catgnav">
                 <?php global $post;             
        $postslist = get_posts( array( 'category_name'=>'technology' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                           <figure class="bsbig_fig wow fadeInDown"> <a class="featured_img"> <?php the_post_thumbnail(); ?> <span class="overlay"></span> </a>
                           <figcaption> <a> <?php the_title(); ?> </a> </figcaption>
                           <p><?php the_excerpt(); ?></p>
                      </li>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
                </ul>
                <ul class="spost_nav">
                  <?php global $post;    
        $postslist = get_posts( array( 'category_name'=>'widget' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                     <div class="media wow fadeInDown"> <a class="media-left"><?php the_post_thumbnail(); ?></a> 
                     <div class="media-body"> <a class="catg_title"><?php the_title(); ?></a></div>
                    </div>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul>
              </div>
            </div>
          </div>
          <div class="single_post_content">
            <h2><span>Photography</span></h2>
            <ul class="photograph_nav  wow fadeInDown">
               <?php global $post;    
        $postslist = get_posts( array( 'category_name'=>'photography' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <div ><figure class="effect-layla">
                          <a class="fancybox-buttons" data-fancybox-group="button" title="Photography Ttile "> <?php the_content();?> </a> </figure> </div>
                      </li>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul>
          </div>
          <div class="single_post_content">
            <h2><span>Games</span></h2>
            <div class="single_post_content_left">
              <ul class="business_catgnav">
                <?php global $post;             
        $postslist = get_posts( array( 'category_name'=>'games' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                      <a class="featured_img"><?php the_post_thumbnail(); ?></a>
                      <h2><?php the_title(); ?></h2>
                      <p><?php the_excerpt(); ?></p>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul>
            </div>
            <div class="single_post_content_right">
              <ul class="spost_nav">
        <?php global $post;    
        $postslist = get_posts( array( 'category_name'=>'widget' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                     <div class="media wow fadeInDown"> <a class="media-left"><?php the_post_thumbnail(); ?></a> 
                     <div class="media-body"> <a class="catg_title"><?php the_title(); ?></a></div>
                    </div>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
              </ul> 
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
                                           <!-- добавить виджет с категориями, любимыми постами, и т.д ( right-foot-widget) -->
        <aside class="right_content">
          <div class="single_sidebar">
            <h2><span>Popular Post</span></h2>
            <ul class="spost_nav">
               <?php global $post;             
        $postslist = get_posts( array( 'category_name'=>'widget' ) );
              foreach ( $postslist as $post ){
                setup_postdata($post);?>
                    <div> 
                      <li>
                        <figure class="bsbig_fig">
                     <div class="media wow fadeInDown"> <a class="media-left"><?php the_post_thumbnail(); ?></a> 
                     <div class="media-body"> <a class="catg_title"><?php the_title(); ?></a></div>
                    </div>
                      </li>
              </figure>
                  </div>
                <?php
              }
              wp_reset_postdata();  ?>
            </ul>
          </div>
          <div class="single_sidebar">
            
              <?php dynamic_sidebar('sidebar-category');?>
        </aside>
      </div>
    </div>
  </section>
   <?php get_footer();?>
</div>
</body>
</html>