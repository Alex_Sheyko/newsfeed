<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'newsfeed' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '}P^0q_*_~ZGh$i /GG[tplv+%.^^>>MYs4WKivR0|+XPgm[u6e:(AOe?t~C2Tr%>' );
define( 'SECURE_AUTH_KEY',  'A4/.uV`((s4.1^)<wqQH+FML#zy:xbG_US%d6`aF?`mSrK.X1P(H3XczJ98uJ+[U' );
define( 'LOGGED_IN_KEY',    ';l7Q:r.RUwBl@|ZM0;jgce~69pjFLletH8eZS<Z: ^%,}f{P0^dbi[31s_#*;K0 ' );
define( 'NONCE_KEY',        'X2E`0QkX}]30;vSIt6?q=eh]L*D-q8|7jSkpN)i=+i/Rd[dXj+2C%.jl)3p:%;As' );
define( 'AUTH_SALT',        '#*h!OcS#lI#sSSf6E[cp46N5.z43HW&&j0-8LUsGt)H8pa>}/{OCBIUvo3&hNUXN' );
define( 'SECURE_AUTH_SALT', 'EcFVNQPNaE+^XA}3I/xDaTv[6}=+^ v5[vcp1C&0{G` Jueu@m7KOzsX5M3x>_q#' );
define( 'LOGGED_IN_SALT',   'ci8l&hV_zgj~WUqo6wsC#1{^&Tsio<UOd36l]Wz[}I+A^B%(d)99>1FYl]B:x.K`' );
define( 'NONCE_SALT',       'FFnN4gDAp0RLG34BuN;(Nx4LgfkV!;vFV p;E)u?};IE(-1,cssu`mTJPkH:mLw$' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
